package com.example.yetanothermetromone.Metronome;


import com.example.yetanothermetromone.Base.AudioGenerator;

public class FrequencyMetronome extends Metronome {

    private int _silence;
    private int _samples = 1000; // samples of tick
    private int _sampleRate = 8000;
    private AudioGenerator audioGenerator;

    public FrequencyMetronome(int bpm, int beat, int timesBeat) {
        super(bpm, beat, timesBeat);

        audioGenerator = new AudioGenerator(_sampleRate);
    }

    @Override
    protected void SetSounds() {
        _tick = audioGenerator.getSineWave(_samples, _sampleRate, 1600);
        _tock = audioGenerator.getSineWave(_samples, _sampleRate, 1000);
    }

    @Override
    public void Start() {
        Play = true;
        double silence = 0;
        double[] sound = new double[1000];
        int t = 0, s = 0, b = 0;
        do
        {
            ProcessTime();
            _silence = CalculateSilence();

            for (int i = 0; i < sound.length; i++)
            {
                if (t < this._samples)
                {
                    if (b == 0)
                        sound[i] = ((double[])_tick)[t];
                    else
                        sound[i] = ((double[])_tock)[t];
                    t++;
                }
                else
                {
                    sound[i] = silence;
                    s++;
                    if (s >= this._silence)
                    {
                        t = 0;
                        s = 0;
                        b++;
                        if (b > (this._timeSignatureTop - 1))
                            b = 0;
                    }
                }
            }
            audioGenerator.writeSound(sound);
        } while (Play);
    }

    @Override
    public void Stop() {
        Play = false;
        audioGenerator.destroyAudioTrack();
    }


    private int CalculateSilence()
    {
        return (int)((((60/(double) _bpm)*8000)-_samples));
    }
}
