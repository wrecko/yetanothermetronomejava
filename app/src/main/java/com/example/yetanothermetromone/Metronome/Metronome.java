package com.example.yetanothermetromone.Metronome;

import android.os.AsyncTask;
import android.util.Log;

import java.sql.Time;

public abstract class Metronome extends AsyncTask<Void, Void, Void> {



    protected Object _tick;
    protected Object _tock;

    protected int _bpm;
    protected int _timeSignatureTop;
    protected int _timeSignatureBot;
    protected abstract void SetSounds();

    protected long StartTime;

    public boolean Play = false;
    public long ElapsedTime;

    public boolean TimerEnabled;
    public long TimerTime;

    public Metronome(int bpm, int beat, int timesBeat)
    {
        _bpm = bpm;
        _timeSignatureTop = beat;
        _timeSignatureBot = timesBeat;
    }

    public Metronome(int bpm, int beat, int timesBeat, long timer)
    {
        _bpm = bpm;
        _timeSignatureTop = beat;
        _timeSignatureBot = timesBeat;
        TimerEnabled = true;
        TimerTime = timer;
    }

    public abstract void Start();
    public abstract void Stop();

    public void SetTempo(int bpm)
    {
        _bpm = bpm;
    }
    public void SetBeatMeasure(int beat)
    {
        _timeSignatureTop = beat;
    }
    public void SetClickTime(int times)
    {
        _timeSignatureBot = times;
//        Log.i("Beat", ""+_timeSignatureBot);
    }

    protected void StartTimer()
    {
        StartTime = System.currentTimeMillis();
    }

    protected void ProcessTime()
    {
        ElapsedTime = System.currentTimeMillis() - StartTime;
        if(TimerEnabled)
        {
            TimerTime -= ElapsedTime;
        }
    }

    @Override
    protected void onPreExecute ()
    {
        this.SetSounds();
    }

    @Override
    protected Void doInBackground(Void... voids)
    {
        StartTimer();
        Start();
        return null;
    }
}
