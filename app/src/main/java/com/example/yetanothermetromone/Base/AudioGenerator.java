package com.example.yetanothermetromone.Base;

import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.AsyncTask;


public class AudioGenerator extends AsyncTask<Void, Void, Void> {
    private int _sampleRate;
    private AudioTrack audioTrack;

    public boolean Play = false;


    public AudioGenerator(int sampleRate)
    {
        _sampleRate = sampleRate;
        audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, _sampleRate, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT, _sampleRate, AudioTrack.MODE_STREAM);
        audioTrack.play();

    }

    public double[] getSineWave(int samples, int sampleRate, double frequencyOfTone)
    {
        double[] sample = new double[samples];
        for (int i = 0; i < samples; i++)
        {
            sample[i] = Math.sin(2 * Math.PI * i / (sampleRate / frequencyOfTone));
        }
        return sample;
    }

    public byte[] get16BitPcm(double[] samples)
    {
        byte[] generatedSound = new byte[2 * samples.length];
        int index = 0;
        for (double sample: samples)
        {
            // scale to maximum amplitude
            short maxSample = (short)((sample * Integer.MAX_VALUE));

            // in 16 bit wav PCM, first byte is the low order byte
            generatedSound[index++] = (byte)(maxSample & 0x00ff);
            generatedSound[index++] = (byte)((maxSample & 0xff00) >> 8);
        }

        return generatedSound;
    }

    public int writeSound(double[] samples)
    {
        byte[] generatedSnd = get16BitPcm(samples);
        return audioTrack.write(generatedSnd, 0, generatedSnd.length);

    }

    public void destroyAudioTrack()
    {
        audioTrack.stop();
        audioTrack.release();
    }

    @Override
    protected Void doInBackground(Void... foo) {
//        audioTrack.play();
//
//        Play = true;
//        boolean tockk = false;
//        while (true)
//        {
//            if(!tockk){
//                writeSound(Tick);
//            }
//            else
//            {
//                writeSound(Tock);
//            }
//            tockk = !tockk;
//
//            try {
//                Thread.sleep((long)(((60 / 100) * 8000) - 1000));
//            } catch(InterruptedException intExc) {
//            }
//        }
        return null;
    }
}
