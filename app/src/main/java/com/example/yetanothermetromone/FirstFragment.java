package com.example.yetanothermetromone;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.yetanothermetromone.Metronome.FrequencyMetronome;
import com.example.yetanothermetromone.Metronome.Metronome;

public class FirstFragment extends Fragment {

    SeekBar bpmSlider;
    TextView bpm_display;
    TextView tempo_name_display;
    private Metronome _metronome;
    private int _maxBpm = 300;
    private int _bpm = 100;
    private int _beat = 4;
    private int _timesBeat = 4;
    private String _timeSignature = "4/4";

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false);
    }


    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/digital.ttf");
        bpm_display = view.findViewById(R.id.bpm_display);
        bpm_display.setTypeface(tf);
        bpm_display.setText(""+ _bpm);

        tempo_name_display = view.findViewById(R.id.tempo_name);
        tempo_name_display.setText(ResolveTempoName(_bpm));

        bpmSlider = view.findViewById(R.id.bpm_slider);
        bpmSlider.setMax(_maxBpm);
        bpmSlider.setProgress(_bpm);
        bpmSlider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                SetBpm(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        view.findViewById(R.id.plus_one).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SetBpm(_bpm+1);
                bpmSlider.setProgress(_bpm);
            }
        });
        view.findViewById(R.id.minus_one).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SetBpm(_bpm-1);
                bpmSlider.setProgress(_bpm);
            }
        });
        view.findViewById(R.id.plus_five).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SetBpm(_bpm+5);
                bpmSlider.setProgress(_bpm);
            }
        });
        view.findViewById(R.id.minus_five).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SetBpm(_bpm-5);
                bpmSlider.setProgress(_bpm);
            }
        });

        final Spinner beat_spinner = view.findViewById(R.id.beat_spinner);
        beat_spinner.setSelection(2);
        beat_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                _beat = Integer.parseInt((String)beat_spinner.getSelectedItem());
                if(_metronome != null)
                    _metronome.SetBeatMeasure(_beat);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final Spinner times_spinner = view.findViewById(R.id.time_spinner);
        beat_spinner.setSelection(2);
        beat_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                _timesBeat = Integer.parseInt((String)times_spinner.getSelectedItem());

                Log.i("Beat", ""+_timesBeat);
                if(_metronome != null)
                    _metronome.SetClickTime(_timesBeat*4);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        view.findViewById(R.id.play_stop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(_metronome == null)
                {
                    _metronome = new FrequencyMetronome(_bpm, _beat, _timesBeat);
                    _metronome.execute();
                }

                if(_metronome.Play)
                {
                    _metronome.Stop();
                    _metronome = null;
                }
            }
        });


        view.findViewById(R.id.timerBtn).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View popupView = inflater.inflate(R.layout.timer_popup, null);

                int width = LinearLayout.LayoutParams.WRAP_CONTENT;
                int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                boolean focusable = true; // lets taps outside the popup also dismiss it
                final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

                // show the popup window
                // which view you pass in doesn't matter, it is only used for the window tolken
                popupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);

                // dismiss the popup window when touched
                popupView.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        popupWindow.dismiss();
                        return true;
                    }
                });

            }
        });
    }

    private void SetBpm(int bpm)
    {
        _bpm = bpm;
        bpm_display.setText("" + bpm);
        if(_metronome != null)
            _metronome.SetTempo(bpm);
        tempo_name_display.setText(ResolveTempoName(_bpm));
    }

    private String ResolveTempoName(int bpm)
    {
        if(bpm <= 39)
            return  "Larghissimo";
        if(bpm > 40 && bpm <= 52)
            return  "Largo";
        if(bpm > 52 && bpm <= 59)
            return  "Largo - Lento";
        if(bpm == 60)
            return  "Largo - Lento - Adagio";
        if(bpm > 60 && bpm <= 68)
            return  "Lento - Adagio";
        if(bpm > 68 && bpm <= 75)
            return  "Adagio";
        if(bpm > 75 && bpm <= 80)
            return  "Adagio - Andante";
        if(bpm > 80 && bpm <= 87)
            return  "Andante";

        if(bpm > 200)
            return "Prestissimo";
        return "";
    }


}
